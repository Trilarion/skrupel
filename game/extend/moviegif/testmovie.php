<?php

$pfad = './';

include "gifencoder.class.php";

if ( $dh = opendir ( $pfad.'files/' ) ) {
	while ( false !== ( $dat = readdir ( $dh ) ) ) {
		if ( $dat != "." && $dat != ".." && $dat != "index.html"  ) {
			$frames [ ] = $pfad."files/$dat";
			$framed [ ] = 120;
		}
	}
	closedir ( $dh );
}

/*
		GIFEncoder constructor:
        =======================

		image_stream = new GIFEncoder	(
							URL or Binary data	'Sources'
							int					'Delay times'
							int					'Animation loops'
							int					'Disposal'
							int					'Transparent red, green, blue colors'
							int					'Source type'
						);
*/
$gif = new GIFEncoder	(
							$frames,
							$framed,
							0,
							2,
							255, 0, 0,
							"url"
		);
/*
		Possibles outputs:
		==================

        Output as GIF for browsers :
        	- Header ( 'Content-type:image/gif' );
        Output as GIF for browsers with filename:
        	- Header ( 'Content-disposition:Attachment;filename=myanimation.gif');
        Output as file to store into a specified file:
        	- FWrite ( FOpen ( "myanimation.gif", "wb" ), $gif->GetAnimation ( ) );
*/
Header ( 'Content-type:image/gif' );
echo	$gif->GetAnimation ( );

?>