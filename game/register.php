<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Skrupel - Tribute Compilation Anmeldung</title>
<META NAME="Author" CONTENT="Bernd Kantoks bernd@kantoks.de">
<meta name="robots" content="index">
<meta name="keywords" content=" ">
<META HTTP-EQUIV="imagetoolbar" CONTENT="no">
<style type="text/css">
BODY,P,TD
{
        font-family:                Verdana;
        font-size:                10px;
         color:                    #ffffff;

  scrollbar-DarkShadow-Color:#444444;
  scrollbar-3dLight-Color:#444444;

  scrollbar-Track-Color:#444444;
  scrollbar-Face-Color:#555555;

  scrollbar-Shadow-Color:#222222;
  scrollbar-Highlight-Color:#888888;

  scrollbar-Arrow-Color:#555555;

}

A
{
		 color:                        #aaaaaa;
       font-weight:                bold;
        text-decoration:        underline;
        }

A:Hover
{
        font-weight:                bold;
        text-decoration:        underline;
        color:                        #ffffff;
}

INPUT,SELECT
{

        background-color:       #555555;
        color:       #ffffff;
        BORDER-BOTTOM-COLOR: #222222;
        BORDER-LEFT-COLOR: #888888;
        BORDER-RIGHT-COLOR: #222222;
        BORDER-TOP-COLOR: #888888;
        Border-Style: solid;
        Border-Width: 1px;
        font-family:                Verdana;
        font-size:                10px;
}
INPUT.eingabe
{

        background-color:       #555555;
        color:       #ffffff;
        BORDER-BOTTOM-COLOR: #888888;
        BORDER-LEFT-COLOR: #222222;
        BORDER-RIGHT-COLOR: #888888;
        BORDER-TOP-COLOR: #222222;
        Border-Style: solid;
        Border-Width: 1px;
        font-family:                Verdana;
        font-size:                10px;
}
</style>
</head>
<body text="#000000" bgcolor="#000000" scroll="no" background="bilder/hintergrund.gif" link="#000000" vlink="#000000" alink="#000000" leftmargin="0" rightmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<center><table border="0" height="100%" cellspacing="0" cellpadding="0">
	<tr>
	  <td><table border="0" cellspacing="0" cellpadding="0" background="bilder/login.gif">
			 <tr>
				<td><img src="../bilder/empty.gif" border="0" width="1" height="1"></td>
				<td><img src="../bilder/empty.gif" border="0" width="628" height="1"></td>
				<td><img src="../bilder/empty.gif" border="0" width="1" height="1"></td>
			 </tr>
			 <tr>
				<td><img src="../bilder/empty.gif" border="0" width="1" height="347"></td>
				<td valign="top"><center>
				<img src="../bilder/empty.gif" border="0" width="1" height="30"><br>
				<img src="bilder/logo_login.gif" width="329" height="208">
        <br>

<?php /////////////////////////////////////////////////////////////////////////////////////////// ?>

<?php
  include ("inc.conf.php"); //Falls dieses Script nicht im Hauptverzeichnis von Skrupel liegt bitte den Pfad entsprechend anpassen

  $conn = @mysql_connect($server.':'.$port,$login,$password);
  $db = @mysql_select_db("$database",$conn);

  $fu=$_GET["fu"];

  if ((!$fu) or ($fu==0)) {
?>

       <table border="0" cellspacing="0" cellpadding="4">
         <tr>
          <td><form action="<?php echo $PHP_SELF.'?fu=1'; ?>" method="post" name="formular"></td>
          <td align="right">Benutzername&nbsp;</td>
          <td><input type="text" name="loginname" class="eingabe" maxlength="30" style="width:350px;" value=""></td>
          <td></td>
				 </tr>
				 <tr>
					<td></td>
          <td align="right">E-Mail&nbsp;</td>
          <td><input type="text" name="email" class="eingabe" maxlength="255" style="width:350px;" value=""></td>
					<td></td>
				 </tr>
         <tr>
					<td></td>
          <td align="right">&nbsp;</td>
          <td><input type="submit" name="submit" value="Anmelden" style="width:350px;"></td>
          <td></form></td>
         </tr>
       </table>

<?php } else {

  $fehlermeldung="";
  $email=$_POST["email"];
  $loginname=$_POST["loginname"];

  if (!strlen($loginname)>=1) { $fehlermeldung=$fehlermeldung."Fehler: Benutzername erforderlich\\n"; }
  if (((strlen($loginname)<4) || (strlen($loginname)>30)) and (strlen($loginname)>=1)) { $fehlermeldung=$fehlermeldung."Fehler: Benutzername darf nur zwischen 4 und 30 Zeichen haben\\n"; }
  if ((!preg_match("/^[a-zA-Z_0-9_\(\)\[\]\-\+\*]+$/",$loginname)) and (strlen($loginname)>=1)) { $fehlermeldung=$fehlermeldung."Fehler: Benutzername muss aus alphanummerischen Zeichen bestehen (0-9,a-z,A-Z)([]()-_+*)\\n"; }

  $zeiger = mysql_query("SELECT count(*) as total FROM $skrupel_user where nick='$loginname'");
  $array = @mysql_fetch_array($zeiger);
  $total=$array["total"];
  if ($total>=1) { $fehlermeldung=$fehlermeldung."Fehler: Benutzername bereits vorhanden\\n";}

  if (!strlen($email)>=1) { $fehlermeldung=$fehlermeldung."Fehler: E-Mail erforderlich\\n"; }

  if (strlen($fehlermeldung)>=1) {
  ?>
  <script language="Javascript">
    alert("<?php echo $fehlermeldung; ?>");
    window.location.href='<?php echo "register.php"; ?>';
  </script>
  <?php
  } else {

  $conso=array("b","c","d","f","g","h","j","k","l","m","n","p","r","s","t","v","w","x","y","z");
  $vocal=array("a","e","i","o","u");
  $passwort="";
  @srand((double)microtime()*1000000);
  for($f=1; $f<=5; $f++) {
     $passwort.=$conso[rand(0,19)];
     $passwort.=$vocal[rand(0,4)];
  }

  $zeiger = @mysql_query("INSERT INTO $skrupel_user (nick,passwort,email,optionen) values ('$loginname','$passwort','$email','00111111111000')");
  $nachricht="Willkommen bei Skrupel - Tribute Compilation\n\nDeine Zugangsdaten lauten\n\nBenutzername: $loginname\nPasswort: $passwort\n\nViel Spass!\n\n------------------------------------------------------------\nDies ist eine automatisch generierte E-Mail\nBitte nicht antworten";
  @mail($email, "S K R U P E L -> Zugangsdaten", $nachricht,"From: $absenderemail\r\n"."Reply-To: $absenderemail\r\n"."X-Mailer: PHP/" . phpversion());

  ?>
        <br>
        Die Anmeldung war erfolgreich.<br><br>
        Die Zugangsdaten wurden per E-Mail �bermittelt.
  <?php
  }
}

@mysql_close();
?>

<?php /////////////////////////////////////////////////////////////////////////////////////////// ?>

        </center></td>
				<td><img src="../bilder/empty.gif" border="0" width="1" height="347"></td>
			 </tr>
			 <tr>
				<td><img src="../bilder/empty.gif" border="0" width="1" height="1"></td>
				<td><img src="../bilder/empty.gif" border="0" width="628" height="1"></td>
				<td><img src="../bilder/empty.gif" border="0" width="1" height="1"></td>
			 </tr>
	  </table></td>
	</tr>
</table></center>
</body>
</html>