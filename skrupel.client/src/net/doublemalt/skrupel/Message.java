package net.doublemalt.skrupel;

import java.io.Serializable;
import java.util.Date;

public abstract class Message implements Serializable
{
    private Long id = null;
    
    private String text = null;
    private int round = 0;
    private Date date = null;
    /**
     * @return Returns the date.
     */
    public Date getDate()
    {
        return date;
    }
    /**
     * @param date The date to set.
     */
    public void setDate(Date date)
    {
        this.date = date;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the round.
     */
    public int getRound()
    {
        return round;
    }
    /**
     * @param round The round to set.
     */
    public void setRound(int round)
    {
        this.round = round;
    }
    /**
     * @return Returns the text.
     */
    public String getText()
    {
        return text;
    }
    /**
     * @param text The text to set.
     */
    public void setText(String text)
    {
        this.text = text;
    }
    
    
}
