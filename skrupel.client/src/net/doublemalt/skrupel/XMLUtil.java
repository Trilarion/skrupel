/*
 * Created on 18.02.2005
 *
 */
package net.doublemalt.skrupel;

import java.io.BufferedOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Vector;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;



/**
 * @author DoubleMalt
 *
 */
public class XMLUtil
{
    public static void writeToFile(Document doc, String fileName)
    {
        OutputFormat of = new OutputFormat();
        
        of.setIndenting(true);
        of.setLineSeparator("\n");
        of.setOmitComments(true);
 

        try {
            FileWriter xmlWriter = new FileWriter(fileName); 
            
            XMLSerializer xml2string  = new XMLSerializer(xmlWriter, of);

            xml2string.serialize(doc);
        } catch (IOException e) 
        {
        }

    }
	
	public static String getAsString(Document doc) 
	{
		String result = null;
    	
		OutputFormat of = new OutputFormat();
        
        of.setIndenting(true);
        of.setLineSeparator("\n");
        of.setOmitComments(true);
        // of.setOmitXMLDeclaration(true);
        // of.setPreserveSpace(true);
        // of.setPreserveEmptyAttributes(true);

        StringWriter xmlWriter = new StringWriter(); 
        
        XMLSerializer xml2string  = new XMLSerializer(xmlWriter, of);

        try {
			xml2string.serialize(doc);
		} catch (IOException e) 
		{
		}
		
        result = xmlWriter.toString();
        
		return result;
	}
	
	public static String getStringValue(Element e)
	{
		String result = "";
		NodeList nl = e.getChildNodes();
		for(int i = 0; i<nl.getLength(); i++)
		{
			if(nl.item(i).getNodeType() == Node.TEXT_NODE)
			{
				result = nl.item(i).getNodeValue();
				break;
			}
		}
		
		
		return result;
	}
	
	public static DocumentBuilder getDB()
	{
		DocumentBuilder result = null;
		
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
       
        dbf.setNamespaceAware(true);

        try
		{
        	result = dbf.newDocumentBuilder();
        }
        catch(ParserConfigurationException e)
		{
        	
		}
        
		
		return result;
	}
    
    public static Vector<String> toStringVector(NodeList nl)
    {
        Vector<String> result = new Vector<String>();
        for (int i = 0; i < nl.getLength(); i++)
        {
            Node node = nl.item(i);
            if(node.getNodeType() == 2)
            {
                result.add(node.getNodeValue());
            }
        }
        return result;
    }
}
