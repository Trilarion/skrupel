package net.doublemalt.skrupel;

import java.io.Serializable;
import java.util.Vector;

public class StarBase  implements Serializable
{
    public enum Type {
        
        Starbase
        
    }
    
    private Long id = null;
    
    private String name = null;
    private int coordX = 0;
    private int coordY = 0;
    private Vector<Attribute> attributes = new Vector<Attribute>();
    
    
    
    
    
    /**
     * @return Returns the attributes.
     */
    public Vector<Attribute> getAttributes()
    {
        return attributes;
    }
    /**
     * @param attributes The attributes to set.
     */
    public void setAttributes(Vector<Attribute> attributes)
    {
        this.attributes = attributes;
    }
    /**
     * @return Returns the coordX.
     */
    public int getCoordX()
    {
        return coordX;
    }
    /**
     * @param coordX The coordX to set.
     */
    public void setCoordX(int coordX)
    {
        this.coordX = coordX;
    }
    /**
     * @return Returns the coordY.
     */
    public int getCoordY()
    {
        return coordY;
    }
    /**
     * @param coordY The coordY to set.
     */
    public void setCoordY(int coordY)
    {
        this.coordY = coordY;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    
    
}
