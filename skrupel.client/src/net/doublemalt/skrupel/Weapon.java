package net.doublemalt.skrupel;

import java.io.Serializable;

public class Weapon implements Serializable
{
    private Long id = null;
    
    private int hullDamage = 0;
    private int crewDamage = 0;
    
    private int techlevel = 0;
    private boolean projectile = false;
    
    private int costCantox = 0;
    private int costBaxterium = 0;
    private int costRennurbin = 0;
    private int costVomissan = 0;
    
    
    
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the costBaxterium.
     */
    public int getCostBaxterium()
    {
        return costBaxterium;
    }
    /**
     * @param costBaxterium The costBaxterium to set.
     */
    public void setCostBaxterium(int costBaxterium)
    {
        this.costBaxterium = costBaxterium;
    }
    /**
     * @return Returns the costCantox.
     */
    public int getCostCantox()
    {
        return costCantox;
    }
    /**
     * @param costCantox The costCantox to set.
     */
    public void setCostCantox(int costCantox)
    {
        this.costCantox = costCantox;
    }
    /**
     * @return Returns the costRennurbin.
     */
    public int getCostRennurbin()
    {
        return costRennurbin;
    }
    /**
     * @param costRennurbin The costRennurbin to set.
     */
    public void setCostRennurbin(int costRennurbin)
    {
        this.costRennurbin = costRennurbin;
    }
    /**
     * @return Returns the costVomissan.
     */
    public int getCostVomissan()
    {
        return costVomissan;
    }
    /**
     * @param costVomissan The costVomissan to set.
     */
    public void setCostVomissan(int costVomissan)
    {
        this.costVomissan = costVomissan;
    }
    /**
     * @return Returns the crewDamage.
     */
    public int getCrewDamage()
    {
        return crewDamage;
    }
    /**
     * @param crewDamage The crewDamage to set.
     */
    public void setCrewDamage(int crewDamage)
    {
        this.crewDamage = crewDamage;
    }
    /**
     * @return Returns the hullDamage.
     */
    public int getHullDamage()
    {
        return hullDamage;
    }
    /**
     * @param hullDamage The hullDamage to set.
     */
    public void setHullDamage(int hullDamage)
    {
        this.hullDamage = hullDamage;
    }
    /**
     * @return Returns the projectile.
     */
    public boolean isProjectile()
    {
        return projectile;
    }
    /**
     * @param projectile The projectile to set.
     */
    public void setProjectile(boolean projectile)
    {
        this.projectile = projectile;
    }
    /**
     * @return Returns the techlevel.
     */
    public int getTechlevel()
    {
        return techlevel;
    }
    /**
     * @param techlevel The techlevel to set.
     */
    public void setTechlevel(int techlevel)
    {
        this.techlevel = techlevel;
    }
    
    

}
