/**
 * 
 */
package net.doublemalt.skrupel;

import java.io.Serializable;
import java.util.Vector;

/**
 * @author DoubleMalt
 *
 */
public class Ship implements Serializable
{
    private Long id = null;
    private ShipType type = null;
    private Vector<Attribute> attributes = new Vector<Attribute>();
    
    private Route route = null;
    private int battleOrder = 0;
    private int projectiles = 0;
    
    private String log = null;

    /**
     * @return Returns the attributes.
     */
    public Vector<Attribute> getAttributes()
    {
        return attributes;
    }

    /**
     * @param attributes The attributes to set.
     */
    public void setAttributes(Vector<Attribute> attributes)
    {
        this.attributes = attributes;
    }

    /**
     * @return Returns the battleOrder.
     */
    public int getBattleOrder()
    {
        return battleOrder;
    }

    /**
     * @param battleOrder The battleOrder to set.
     */
    public void setBattleOrder(int battleOrder)
    {
        this.battleOrder = battleOrder;
    }

    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return Returns the log.
     */
    public String getLog()
    {
        return log;
    }

    /**
     * @param log The log to set.
     */
    public void setLog(String log)
    {
        this.log = log;
    }

    /**
     * @return Returns the projectiles.
     */
    public int getProjectiles()
    {
        return projectiles;
    }

    /**
     * @param projectiles The projectiles to set.
     */
    public void setProjectiles(int projectiles)
    {
        this.projectiles = projectiles;
    }

    /**
     * @return Returns the route.
     */
    public Route getRoute()
    {
        return route;
    }

    /**
     * @param route The route to set.
     */
    public void setRoute(Route route)
    {
        this.route = route;
    }

    /**
     * @return Returns the type.
     */
    public ShipType getType()
    {
        return type;
    }

    /**
     * @param type The type to set.
     */
    public void setType(ShipType type)
    {
        this.type = type;
    }
    
    
    
    
}
