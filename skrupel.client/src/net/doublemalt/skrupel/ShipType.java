package net.doublemalt.skrupel;

import java.io.Serializable;

/**
 * @author DoubleMalt
 *
 */
public class ShipType implements Serializable
{
    private Long id = null;
    private String name = null;
    
    private int techlevel = 0;
    private int engines = 0;
    private int energyWeapons = 0;
    private int projectileWeapons = 0;
    private int mass = 0;
    private int crew = 0;
    
    private int costCantox = 0;
    private int costBaxterium = 0;
    private int costRennurbin = 0;
    private int costVomissan = 0;
    
    /**
     * @return Returns the crew.
     */
    public int getCrew()
    {
        return crew;
    }
    /**
     * @param crew The crew to set.
     */
    public void setCrew(int crew)
    {
        this.crew = crew;
    }
    /**
     * @return Returns the energyWeapons.
     */
    public int getEnergyWeapons()
    {
        return energyWeapons;
    }
    /**
     * @param energyWeapons The energyWeapons to set.
     */
    public void setEnergyWeapons(int energyWeapons)
    {
        this.energyWeapons = energyWeapons;
    }
    /**
     * @return Returns the engines.
     */
    public int getEngines()
    {
        return engines;
    }
    /**
     * @param engines The engines to set.
     */
    public void setEngines(int engines)
    {
        this.engines = engines;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the mass.
     */
    public int getMass()
    {
        return mass;
    }
    /**
     * @param mass The mass to set.
     */
    public void setMass(int mass)
    {
        this.mass = mass;
    }
    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }
    /**
     * @return Returns the projectileWeapons.
     */
    public int getProjectileWeapons()
    {
        return projectileWeapons;
    }
    /**
     * @param projectileWeapons The projectileWeapons to set.
     */
    public void setProjectileWeapons(int projectileWeapons)
    {
        this.projectileWeapons = projectileWeapons;
    }
    /**
     * @return Returns the techlevel.
     */
    public int getTechlevel()
    {
        return techlevel;
    }
    /**
     * @param techlevel The techlevel to set.
     */
    public void setTechlevel(int techlevel)
    {
        this.techlevel = techlevel;
    }
    /**
     * @return Returns the costBaxterium.
     */
    public int getCostBaxterium()
    {
        return costBaxterium;
    }
    /**
     * @param costBaxterium The costBaxterium to set.
     */
    public void setCostBaxterium(int costBaxterium)
    {
        this.costBaxterium = costBaxterium;
    }
    /**
     * @return Returns the costCantox.
     */
    public int getCostCantox()
    {
        return costCantox;
    }
    /**
     * @param costCantox The costCantox to set.
     */
    public void setCostCantox(int costCantox)
    {
        this.costCantox = costCantox;
    }
    /**
     * @return Returns the costRennurbin.
     */
    public int getCostRennurbin()
    {
        return costRennurbin;
    }
    /**
     * @param costRennurbin The costRennurbin to set.
     */
    public void setCostRennurbin(int costRennurbin)
    {
        this.costRennurbin = costRennurbin;
    }
    /**
     * @return Returns the costVomissan.
     */
    public int getCostVomissan()
    {
        return costVomissan;
    }
    /**
     * @param costVomissan The costVomissan to set.
     */
    public void setCostVomissan(int costVomissan)
    {
        this.costVomissan = costVomissan;
    }
    
    
   
}
