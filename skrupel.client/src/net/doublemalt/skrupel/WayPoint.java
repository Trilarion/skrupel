package net.doublemalt.skrupel;

import java.io.Serializable;

public class WayPoint implements Serializable
{
    private Long id = null;
    
    public enum Action
    {
        Load,
        Keep,
        Unload
    }
    
    private Planet planet = null;
    
    private Action leminAction = Action.Keep;
    private Action rennurbinAction = Action.Keep;
    private Action baxteriumAction = Action.Keep;
    private Action vomissanAction = Action.Keep;
    private Action suppliesAction = Action.Keep;
    private Action cantoxAction = Action.Keep;
    /**
     * @return Returns the baxteriumAction.
     */
    public Action getBaxteriumAction()
    {
        return baxteriumAction;
    }
    /**
     * @param baxteriumAction The baxteriumAction to set.
     */
    public void setBaxteriumAction(Action baxteriumAction)
    {
        this.baxteriumAction = baxteriumAction;
    }
    /**
     * @return Returns the cantoxAction.
     */
    public Action getCantoxAction()
    {
        return cantoxAction;
    }
    /**
     * @param cantoxAction The cantoxAction to set.
     */
    public void setCantoxAction(Action cantoxAction)
    {
        this.cantoxAction = cantoxAction;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the leminAction.
     */
    public Action getLeminAction()
    {
        return leminAction;
    }
    /**
     * @param leminAction The leminAction to set.
     */
    public void setLeminAction(Action leminAction)
    {
        this.leminAction = leminAction;
    }
    /**
     * @return Returns the planet.
     */
    public Planet getPlanet()
    {
        return planet;
    }
    /**
     * @param planet The planet to set.
     */
    public void setPlanet(Planet planet)
    {
        this.planet = planet;
    }
    /**
     * @return Returns the rennurbinAction.
     */
    public Action getRennurbinAction()
    {
        return rennurbinAction;
    }
    /**
     * @param rennurbinAction The rennurbinAction to set.
     */
    public void setRennurbinAction(Action rennurbinAction)
    {
        this.rennurbinAction = rennurbinAction;
    }
    /**
     * @return Returns the suppliesAction.
     */
    public Action getSuppliesAction()
    {
        return suppliesAction;
    }
    /**
     * @param suppliesAction The suppliesAction to set.
     */
    public void setSuppliesAction(Action suppliesAction)
    {
        this.suppliesAction = suppliesAction;
    }
    /**
     * @return Returns the vomissanAction.
     */
    public Action getVomissanAction()
    {
        return vomissanAction;
    }
    /**
     * @param vomissanAction The vomissanAction to set.
     */
    public void setVomissanAction(Action vomissanAction)
    {
        this.vomissanAction = vomissanAction;
    }
    
    

}
