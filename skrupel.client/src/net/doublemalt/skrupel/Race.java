package net.doublemalt.skrupel;

import java.io.Serializable;
import java.util.Vector;


/**
 * @author DoubleMalt
 *
 */
public class Race implements Serializable
{
    private Long id = null;
    
    private String shortName = null;
    private String name = null;
    private String description = null;
    
    private double attack = 0;
    private double defense = 0;
    
    private double productivity = 0;
    private double mining = 0;
    private double taxes = 0;
    
    private int preferredTemperature = 0;
    private PlanetClass preferredPlanetClass = null;
    
    private Vector<ShipType> shipTypes = new Vector<ShipType>();
    
    private Vector<OrbitalSystem> orbitalSystems = new Vector<OrbitalSystem>();

    /**
     * @return Returns the attack.
     */
    public double getAttack()
    {
        return attack;
    }

    /**
     * @param attack The attack to set.
     */
    public void setAttack(double attack)
    {
        this.attack = attack;
    }

    /**
     * @return Returns the defense.
     */
    public double getDefense()
    {
        return defense;
    }

    /**
     * @param defense The defense to set.
     */
    public void setDefense(double defense)
    {
        this.defense = defense;
    }

    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return Returns the mining.
     */
    public double getMining()
    {
        return mining;
    }

    /**
     * @param mining The mining to set.
     */
    public void setMining(double mining)
    {
        this.mining = mining;
    }

    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return Returns the preferredPlanetClass.
     */
    public PlanetClass getPreferredPlanetClass()
    {
        return preferredPlanetClass;
    }

    /**
     * @param preferredPlanetClass The preferredPlanetClass to set.
     */
    public void setPreferredPlanetClass(PlanetClass preferredPlanetClass)
    {
        this.preferredPlanetClass = preferredPlanetClass;
    }

    /**
     * @return Returns the preferredTemperature.
     */
    public int getPreferredTemperature()
    {
        return preferredTemperature;
    }

    /**
     * @param preferredTemperature The preferredTemperature to set.
     */
    public void setPreferredTemperature(int preferredTemperature)
    {
        this.preferredTemperature = preferredTemperature;
    }

    /**
     * @return Returns the productivity.
     */
    public double getProductivity()
    {
        return productivity;
    }

    /**
     * @param productivity The productivity to set.
     */
    public void setProductivity(double productivity)
    {
        this.productivity = productivity;
    }

    /**
     * @return Returns the shortName.
     */
    public String getShortName()
    {
        return shortName;
    }

    /**
     * @param shortName The shortName to set.
     */
    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    /**
     * @return Returns the taxes.
     */
    public double getTaxes()
    {
        return taxes;
    }

    /**
     * @param taxes The taxes to set.
     */
    public void setTaxes(double taxes)
    {
        this.taxes = taxes;
    }

    /**
     * @return Returns the orbitalSystems.
     */
    public Vector<OrbitalSystem> getOrbitalSystems()
    {
        return orbitalSystems;
    }

    /**
     * @return Returns the shipTypes.
     */
    public Vector<ShipType> getShipTypes()
    {
        return shipTypes;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    
    
}
