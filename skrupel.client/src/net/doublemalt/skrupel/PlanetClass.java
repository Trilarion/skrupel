package net.doublemalt.skrupel;

import java.io.Serializable;

public class PlanetClass implements Serializable
{
    private Long id = null;
    
    private String name = null;
    private int upperBound = 0;
    private int lowerBound = 0;
    
    private String atmosphere = null;
    private String surface = null;
    /**
     * @return Returns the atmosphere.
     */
    public String getAtmosphere()
    {
        return atmosphere;
    }
    /**
     * @param atmosphere The atmosphere to set.
     */
    public void setAtmosphere(String atmosphere)
    {
        this.atmosphere = atmosphere;
    }
    /**
     * @return Returns the lowerBound.
     */
    public int getLowerBound()
    {
        return lowerBound;
    }
    /**
     * @param lowerBound The lowerBound to set.
     */
    public void setLowerBound(int lowerBound)
    {
        this.lowerBound = lowerBound;
    }
    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }
    /**
     * @return Returns the surface.
     */
    public String getSurface()
    {
        return surface;
    }
    /**
     * @param surface The surface to set.
     */
    public void setSurface(String surface)
    {
        this.surface = surface;
    }
    /**
     * @return Returns the upperBound.
     */
    public int getUpperBound()
    {
        return upperBound;
    }
    /**
     * @param upperBound The upperBound to set.
     */
    public void setUpperBound(int upperBound)
    {
        this.upperBound = upperBound;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    
    
    
    

}
