package net.doublemalt.skrupel;

import java.io.Serializable;



public class Player implements Serializable
{
    private Long id = null;
    private String name = null;
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    
}
