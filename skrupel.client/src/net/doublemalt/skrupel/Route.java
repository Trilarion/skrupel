package net.doublemalt.skrupel;

import java.io.Serializable;
import java.util.Vector;

public class Route implements Serializable
{
    private Long id = null;
    
    private Vector<WayPoint> wayPoints = new Vector<WayPoint>();
    
    private int speed = 0;
    private int minFuel = 0;
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the minFuel.
     */
    public int getMinFuel()
    {
        return minFuel;
    }
    /**
     * @param minFuel The minFuel to set.
     */
    public void setMinFuel(int minFuel)
    {
        this.minFuel = minFuel;
    }
    /**
     * @return Returns the speed.
     */
    public int getSpeed()
    {
        return speed;
    }
    /**
     * @param speed The speed to set.
     */
    public void setSpeed(int speed)
    {
        this.speed = speed;
    }
    /**
     * @return Returns the wayPoints.
     */
    public Vector<WayPoint> getWayPoints()
    {
        return wayPoints;
    }
    /**
     * @param wayPoints The wayPoints to set.
     */
    public void setWayPoints(Vector<WayPoint> wayPoints)
    {
        this.wayPoints = wayPoints;
    }
    
    
    
    
}
