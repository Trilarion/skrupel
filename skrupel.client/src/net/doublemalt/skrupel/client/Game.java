/**
 * 
 */
package net.doublemalt.skrupel.client;

import java.io.Serializable;
import java.util.Vector;

import net.doublemalt.skrupel.Player;


/**
 * @author DoubleMalt
 *
 */
public class Game implements Serializable
{
    private Long id = null;
    
    public enum VictoryCondition
    {
        Survival,
        Enemy,
        JustForFun
    }
    
    private int gameID = 0;
    private GameStatistics stats = null;
    
    private int currentRound = 0;
    
    private Map map = null;
    
    private Vector<Opponent> opponents = null;
    
    private VictoryCondition victoryCondition = null;
    
    private int victoryParameter = 0;

    /**
     * @return Returns the currentRound.
     */
    public int getCurrentRound()
    {
        return currentRound;
    }

    /**
     * @param currentRound The currentRound to set.
     */
    public void setCurrentRound(int currentRound)
    {
        this.currentRound = currentRound;
    }

    /**
     * @return Returns the gameID.
     */
    public int getGameID()
    {
        return gameID;
    }

    /**
     * @param gameID The gameID to set.
     */
    public void setGameID(int gameID)
    {
        this.gameID = gameID;
    }

    /**
     * @return Returns the map.
     */
    public Map getMap()
    {
        return map;
    }

    /**
     * @param map The map to set.
     */
    public void setMap(Map map)
    {
        this.map = map;
    }


    /**
     * @return Returns the stats.
     */
    public GameStatistics getStats()
    {
        return stats;
    }

    /**
     * @param stats The stats to set.
     */
    public void setStats(GameStatistics stats)
    {
        this.stats = stats;
    }

    /**
     * @return Returns the victoryCondition.
     */
    public VictoryCondition getVictoryCondition()
    {
        return victoryCondition;
    }

    /**
     * @param victoryCondition The victoryCondition to set.
     */
    public void setVictoryCondition(VictoryCondition victoryCondition)
    {
        this.victoryCondition = victoryCondition;
    }

    /**
     * @return Returns the victoryParameter.
     */
    public int getVictoryParameter()
    {
        return victoryParameter;
    }

    /**
     * @param victoryParameter The victoryParameter to set.
     */
    public void setVictoryParameter(int victoryParameter)
    {
        this.victoryParameter = victoryParameter;
    }

    /**
     * @return Returns the players.
     */
    public Vector<Opponent> getOpponents()
    {
        return opponents;
    }

    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @param players The players to set.
     */
    public void setPlayers(Vector<Opponent> opponents)
    {
        this.opponents = opponents;
    }
    
    
    
    
    
    
    
    

}
