package net.doublemalt.skrupel.client;

import java.io.Serializable;

import net.doublemalt.skrupel.Player;
import net.doublemalt.skrupel.Race;

public class Opponent implements Serializable
{

    public enum PlayerRelation 
    {
        SELF,ALLY,FRIEND,PEACE,TRADE,VOID,WAR
    }
    
    private Long id = null;
    
    private Player player = null;
    private PlayerRelation relation = PlayerRelation.VOID;
    private Race race = null;
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the player.
     */
    public Player getPlayer()
    {
        return player;
    }
    /**
     * @param player The player to set.
     */
    public void setPlayer(Player player)
    {
        this.player = player;
    }
    /**
     * @return Returns the race.
     */
    public Race getRace()
    {
        return race;
    }
    /**
     * @param race The race to set.
     */
    public void setRace(Race race)
    {
        this.race = race;
    }
    /**
     * @return Returns the relation.
     */
    public PlayerRelation getRelation()
    {
        return relation;
    }
    /**
     * @param relation The relation to set.
     */
    public void setRelation(PlayerRelation relation)
    {
        this.relation = relation;
    }

    
}
