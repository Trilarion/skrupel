package net.doublemalt.skrupel.client.html;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.doublemalt.skrupel.PlanetClass;
import net.doublemalt.skrupel.Race;
import net.doublemalt.skrupel.XMLUtil;
import net.doublemalt.skrupel.client.Game;
import net.doublemalt.skrupel.client.SkrupelServer;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.ccil.cowan.tagsoup.Parser;
import org.ccil.cowan.tagsoup.XMLWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.sun.org.apache.bcel.internal.verifier.statics.DOUBLE_Upper;


public class Connector
{
    private static Logger log = Logger.getLogger(Connector.class);
    
    
    private DocumentBuilder db = null;
    private HttpClient client = null;
    private XPath myXPath = XPathFactory.newInstance().newXPath();

    
    public Connector()
    {
        log.info("Init HtmlConnector ... ");
        log.info(log.isDebugEnabled()+"");
        client = new HttpClient(new MultiThreadedHttpConnectionManager());
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);

        try
        {
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        }
        catch (ParserConfigurationException e)
        {
            log.error("Couldn't initialize XMLParser.", e);
        }
    }
    
    public void getServerData(SkrupelServer server)
    {
        String urlstring = null;
        
        
        //////////////////////////
        //
        //  Log in
        //
        
        if(server.getGames().size() < 1)
        {
            Document loginPage = get(server.getUrl().toString()+"skrupel.php");
            try
            {
                NodeList gameIDs = (NodeList) myXPath.evaluate("//option/@value", loginPage, XPathConstants.NODESET);
                
                boolean dataFetched = false;
                
                for (int i = 1; i< gameIDs.getLength(); i++)
                {
                    Node id = gameIDs.item(i);
                    
                    Vector<NameValuePair> postVars = new Vector<NameValuePair>();
                    postVars.add(new NameValuePair("login_f", server.getUserName()));
                    postVars.add(new NameValuePair("passwort_f", server.getPassWord()));
                    postVars.add(new NameValuePair("spiel_slot", id.getNodeValue()));
                    
                    Document frameSet = post(server.getUrl().toString()+"skrupel.php", postVars);
                    
                    String test = myXPath.evaluate("/html[frameset]", frameSet);
                    
                    // log.info(test);
                    
                    if(!test.equals(""))
                    {
                        Game game = new Game();
                        game.setGameID(Integer.parseInt(id.getNodeValue()));
                        String menuString = null;
                        String idString = null;
                        
                        if(!dataFetched)
                        {
                            try
                            {
                                menuString = myXPath.evaluate("//frame[@name='mittelinksoben']/@src", frameSet);
                                idString = menuString.substring(21);
                            }
                            catch (XPathExpressionException e)
                            {
                                log.error("Couldn't read idString", e);
                            }
                            //////
                            // Get Planet Information
                            //
                            log.info("Fetching planet data ...");
                            
                            
                            urlstring = server.getUrl() + "inhalt/meta_pklassen.php?fu=1&" + idString;
                            Document planetDoc = get(urlstring);
                            
                            String xpathString = "/html/body/center/table/tr[position() mod 3  = 1]/td";
                            NodeList planetList = (NodeList) myXPath.evaluate(xpathString, planetDoc, XPathConstants.NODESET);
                            
                            
                            
                            for (int j = 0; j < planetList.getLength(); j++)
                            {
                                Node curPlan = planetList.item(j);
                                PlanetClass pc = new PlanetClass();
                                
                                String planetName = curPlan.getTextContent();
                                
                                if(planetName.length() > 7)
                                {
 
                                    pc.setName(planetName.substring(7));
                                    
                                    xpathString = "/html/body/center/table/tr[position()=" + ((Math.round(Math.floor(j/2))+1)*3) + "]/td[position()=" + (2*((j % 2) + 1)) +"]";
                                    Node dataNode = (Node) myXPath.evaluate(xpathString, curPlan, XPathConstants.NODE);
                                    
                                    xpathString = "table/tr[position()=2]/td[position()=2]";
                                    pc.setSurface(myXPath.evaluate(xpathString, dataNode));
                                    
                                    xpathString = "table/tr[position()=4]/td[position()=2]";
                                    pc.setAtmosphere(myXPath.evaluate(xpathString, dataNode));
                                    
                                    xpathString = "substring-before(string(table/tr[position()=6]/td[position()=2]),' bis ')";
                                    pc.setLowerBound(Integer.parseInt(myXPath.evaluate(xpathString, dataNode)));
                                    
                                    xpathString = "substring-before(substring-after(string(table/tr[position()=6]/td[position()=2]),' bis '),' Grad')";
                                    pc.setLowerBound(Integer.parseInt(myXPath.evaluate(xpathString, dataNode)));

                                    
                                 }
                                
                                
                                
                                
                            }
                                        
                            //////
                            // Get Race Informations
                            
                            log.info("Fetching Race data ...");
                            
                            urlstring = server.getUrl() + "inhalt/meta_rassen.php?fu=1&"+idString;
                            
                            // log.debug(urlstring);
                            
                            Document raceListDoc = get(urlstring);
                            
                            // log.debug(XMLUtil.getAsString(raceListDoc));
                            
                            xpathString = "//a/@onclick";
                            
                            Vector<String> raceLinks = XMLUtil.toStringVector((NodeList) myXPath.evaluate(xpathString, raceListDoc, XPathConstants.NODESET));
                            
                            Pattern pStart = Pattern.compile(".*rasse=");
                            Pattern pEnd = Pattern.compile("'\\).*");
                            
                            for (String raceLink : raceLinks)
                            {
                                
                                Matcher m = pStart.matcher(raceLink);
                                m=pEnd.matcher(m.replaceAll(""));
                                String raceName = m.replaceAll("");
                                
                                Race race = new Race();
                                race.setShortName(raceName);
                                
                                server.getRaces().add(race);
                                
                                String raceUrl = server.getUrl() + "inhalt/meta_rassen.php?fu=2&" + idString + "&rasse=" + raceName;
                                
                                Document raceDoc = get(raceUrl);
                                
                                xpathString = "/html/body/table/tr/td/center/b";
                                race.setName(myXPath.evaluate(xpathString, raceDoc));
                                
                                xpathString = "/html/body/table[position()=2]/tr[position()=2]/td";
                                race.setDescription(myXPath.evaluate(xpathString, raceDoc));
                                
                                xpathString = "/html/body/table[position()=2]/tr[position()=2]/td";
                                race.setDescription(myXPath.evaluate(xpathString, raceDoc));
                                
                                
                                if(raceName.equals("isa"))
                                {
                                    XMLUtil.writeToFile(raceDoc, "race(isa).xml");
                                }
                            }
                             
                            
                            
                            

                            
                        }
                        
                        dataFetched = true;
                    }
    
                    
                }
            }
            catch (XPathExpressionException e)
            {
                log.error("Some XPath Error", e);
            }
        }
        
    }
    
    
    public void updateGameData(SkrupelServer server, Game game)
    {
        String urlstring = null;
        
        //////////////////////////
        //
        //  Log in
        //
        
        Vector<NameValuePair> postVars = new Vector<NameValuePair>();
        postVars.add(new NameValuePair("login_f", server.getUserName()));
        postVars.add(new NameValuePair("passwort_f", server.getPassWord()));
        postVars.add(new NameValuePair("spiel_slot", (new Integer(game.getGameID())).toString()));
        
        Document frameSet = post(server.getUrl().toString()+"skrupel.php", postVars);
        
        String menuString = "";
        String idString = "";
        try
        {
            menuString = myXPath.evaluate("//frame[@name='mittelinksoben']/@src", frameSet);
        }
        catch (XPathExpressionException e)
        {
            log.error("Couldn't read idString", e);
        }
        
        idString = menuString.substring(21);
        
        log.info(idString);
        
        // DEV - fetch menu
        urlstring = server.getUrl().toString() + menuString;
        
        Document menudoc = get(urlstring);
       
        /////////////////////////////
        //
        // Fetch Starbases
        //
        urlstring = server.getUrl().toString() + "inhalt/basen.php?fu=1&" + idString;
        
        Document basedoc = get(urlstring);
        
        //////////////////////////////////////////////////
        //
        // Fetch Game Information
        //
        
        urlstring = server.getUrl().toString() + "inhalt/uebersicht_imperien.php?fu=1&" + idString;
        
        Document playerdoc = get(urlstring);
        
        ////////////////////////////////////////////////////
        //
        // Fetch Map
        //
        
        urlstring = server.getUrl().toString() + "inhalt/galaxie.php?" + idString;
        
        Document mapdoc = get(urlstring);

        /////////////////////////////////////////////////////
        //
        // Fetch Ships
        //
        
        urlstring = server.getUrl().toString() + "inhalt/flotte.php?fu=1&" + idString;
        
        Document shipdoc = get(urlstring);

    }
    
    private Document get(String urlString)
    {
        Document result = null;
        
        try
        {
            GetMethod getfetcher = new GetMethod(urlString);
            client.executeMethod(getfetcher);
            
            XMLReader r = new Parser();
            InputSource s = new InputSource(getfetcher.getResponseBodyAsStream());
            StringWriter w = new StringWriter();
            XMLWriter h = new XMLWriter(w);
            
            r.setContentHandler(h);
            r.parse(s);
            InputSource is2 = new InputSource(new StringReader(w.toString()));
            result = db.parse(is2);
            
            log.debug(XMLUtil.getAsString(result));
        }
        catch (HttpException e)
        {
            log.error("Couldn't get File", e);
        }
        catch (IOException e)
        {
            log.error("Couldn't read Input", e);
        }
        catch (SAXException e)
        {
            log.error("Couldn't parse", e);
        }
        return result;
    }
    
    private Document post(String urlString, Vector<NameValuePair> postValues)
    {
        Document result = null;
        try
        {
            PostMethod postfetcher = new PostMethod(urlString);
            for (NameValuePair pair : postValues)
            {
                postfetcher.addParameter(pair);
            }
            
            
            client.executeMethod(postfetcher);
            
            XMLReader r = new Parser();
            InputSource s = new InputSource(postfetcher.getResponseBodyAsStream());
            StringWriter w = new StringWriter();
            XMLWriter h = new XMLWriter(w);
            
            r.setContentHandler(h);
            r.parse(s);
            InputSource is2 = new InputSource(new StringReader(w.toString()));
            result = db.parse(is2);
            
            log.debug(XMLUtil.getAsString(result));
        }
        catch (HttpException e)
        {
            log.error("Couldn't get File", e);
        }
        catch (IOException e)
        {
            log.error("Couldn't read Input", e);
        }
        catch (SAXException e)
        {
            log.error("Couldn't parse", e);
        }
        return result;
    }
    
    

}
