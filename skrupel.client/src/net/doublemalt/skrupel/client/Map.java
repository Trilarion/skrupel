package net.doublemalt.skrupel.client;

import java.io.Serializable;
import java.util.Vector;

import net.doublemalt.skrupel.Planet;
import net.doublemalt.skrupel.Ship;
import net.doublemalt.skrupel.StarBase;
import net.doublemalt.skrupel.WormHole;

public class Map  implements Serializable
{
    private Long id = null;
    
    private Vector<Planet> planets = new Vector<Planet>();
    private Vector<Ship> ships = new Vector<Ship>();
    private Vector<WormHole> wormHoles = new Vector<WormHole>();
    private Vector<StarBase> starBases = new Vector<StarBase>();
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the planets.
     */
    public Vector<Planet> getPlanets()
    {
        return planets;
    }
    /**
     * @param planets The planets to set.
     */
    public void setPlanets(Vector<Planet> planets)
    {
        this.planets = planets;
    }
    /**
     * @return Returns the ships.
     */
    public Vector<Ship> getShips()
    {
        return ships;
    }
    /**
     * @param ships The ships to set.
     */
    public void setShips(Vector<Ship> ships)
    {
        this.ships = ships;
    }
    /**
     * @return Returns the starBases.
     */
    public Vector<StarBase> getStarBases()
    {
        return starBases;
    }
    /**
     * @param starBases The starBases to set.
     */
    public void setStarBases(Vector<StarBase> starBases)
    {
        this.starBases = starBases;
    }
    /**
     * @return Returns the wormHoles.
     */
    public Vector<WormHole> getWormHoles()
    {
        return wormHoles;
    }
    /**
     * @param wormHoles The wormHoles to set.
     */
    public void setWormHoles(Vector<WormHole> wormHoles)
    {
        this.wormHoles = wormHoles;
    }
    
    

}
