/**
 * 
 */
package net.doublemalt.skrupel.client;

import java.io.Serializable;
import java.net.URL;
import java.util.Vector;

import net.doublemalt.skrupel.DomSpecies;
import net.doublemalt.skrupel.Race;

/**
 * @author DoubleMalt
 *
 */
public class SkrupelServer implements Serializable 
{
    private Long id = null;
    
	private String type = null;
	private URL url = null;
    private String userName = null;
    private String passWord = null;
    
    private Vector<DomSpecies> domSpecies = new Vector<DomSpecies>();
    private Vector<Race> races = new Vector<Race>();
    
    private Vector<Game> games = new Vector<Game>();
    
    /**
     * @return Returns the passWord.
     */
    public String getPassWord()
    {
        return passWord;
    }
    /**
     * @param passWord The passWord to set.
     */
    public void setPassWord(String passWord)
    {
        this.passWord = passWord;
    }
    /**
     * @return Returns the type.
     */
    public String getType()
    {
        return type;
    }
    /**
     * @param type The type to set.
     */
    public void setType(String type)
    {
        this.type = type;
    }
    /**
     * @return Returns the url.
     */
    public URL getUrl()
    {
        return url;
    }
    /**
     * @param url The url to set.
     */
    public void setUrl(URL url)
    {
        this.url = url;
    }
    /**
     * @return Returns the userName.
     */
    public String getUserName()
    {
        return userName;
    }
    /**
     * @param userName The userName to set.
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    /**
     * @return Returns the domSpecies.
     */
    public Vector<DomSpecies> getDomSpecies()
    {
        return domSpecies;
    }
    /**
     * @return Returns the races.
     */
    public Vector<Race> getRaces()
    {
        return races;
    }
    /**
     * @return Returns the games.
     */
    public Vector<Game> getGames()
    {
        return games;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @param domSpecies The domSpecies to set.
     */
    public void setDomSpecies(Vector<DomSpecies> domSpecies)
    {
        this.domSpecies = domSpecies;
    }
    /**
     * @param games The games to set.
     */
    public void setGames(Vector<Game> games)
    {
        this.games = games;
    }
    /**
     * @param races The races to set.
     */
    public void setRaces(Vector<Race> races)
    {
        this.races = races;
    }

    
    
    
}
