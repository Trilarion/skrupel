package net.doublemalt.skrupel.client;

import java.io.Serializable;

/**
 * @author DoubleMalt
 *
 */
public class GameStatistics implements Serializable
{
    private Long id = null;

    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    
    
}
