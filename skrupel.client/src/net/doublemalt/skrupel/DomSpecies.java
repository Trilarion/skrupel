package net.doublemalt.skrupel;

import java.io.Serializable;

public class DomSpecies implements Serializable
{
    private Long id = null;
    private String name = null;
    
    private double taxes = 0;
    
    private double attack = 0;
    
    private double defenseGroundMod = 0;
    private double defenseSpaceMod = 0;
    
    private double productivity = 0;
    private double growth = 0;
    private double mining = 0;
    
    private int tlHull = 0;
    private int tlEnWeapons = 0;
    private int tlPrWeapons = 0;
    private int tlEngines = 0;
    /**
     * @return Returns the attack.
     */
    public double getAttack()
    {
        return attack;
    }
    /**
     * @param attack The attack to set.
     */
    public void setAttack(double attack)
    {
        this.attack = attack;
    }
    /**
     * @return Returns the defenseGroundMod.
     */
    public double getDefenseGroundMod()
    {
        return defenseGroundMod;
    }
    /**
     * @param defenseGroundMod The defenseGroundMod to set.
     */
    public void setDefenseGroundMod(double defenseGroundMod)
    {
        this.defenseGroundMod = defenseGroundMod;
    }
    /**
     * @return Returns the defenseSpaceMod.
     */
    public double getDefenseSpaceMod()
    {
        return defenseSpaceMod;
    }
    /**
     * @param defenseSpaceMod The defenseSpaceMod to set.
     */
    public void setDefenseSpaceMod(double defenseSpaceMod)
    {
        this.defenseSpaceMod = defenseSpaceMod;
    }
    /**
     * @return Returns the growth.
     */
    public double getGrowth()
    {
        return growth;
    }
    /**
     * @param growth The growth to set.
     */
    public void setGrowth(double growth)
    {
        this.growth = growth;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the mining.
     */
    public double getMining()
    {
        return mining;
    }
    /**
     * @param mining The mining to set.
     */
    public void setMining(double mining)
    {
        this.mining = mining;
    }
    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }
    /**
     * @return Returns the productivity.
     */
    public double getProductivity()
    {
        return productivity;
    }
    /**
     * @param productivity The productivity to set.
     */
    public void setProductivity(double productivity)
    {
        this.productivity = productivity;
    }
    /**
     * @return Returns the taxes.
     */
    public double getTaxes()
    {
        return taxes;
    }
    /**
     * @param taxes The taxes to set.
     */
    public void setTaxes(double taxes)
    {
        this.taxes = taxes;
    }
    /**
     * @return Returns the tlEngines.
     */
    public int getTlEngines()
    {
        return tlEngines;
    }
    /**
     * @param tlEngines The tlEngines to set.
     */
    public void setTlEngines(int tlEngines)
    {
        this.tlEngines = tlEngines;
    }
    /**
     * @return Returns the tlEnWeapons.
     */
    public int getTlEnWeapons()
    {
        return tlEnWeapons;
    }
    /**
     * @param tlEnWeapons The tlEnWeapons to set.
     */
    public void setTlEnWeapons(int tlEnWeapons)
    {
        this.tlEnWeapons = tlEnWeapons;
    }
    /**
     * @return Returns the tlHull.
     */
    public int getTlHull()
    {
        return tlHull;
    }
    /**
     * @param tlHull The tlHull to set.
     */
    public void setTlHull(int tlHull)
    {
        this.tlHull = tlHull;
    }
    /**
     * @return Returns the tlPrWeapons.
     */
    public int getTlPrWeapons()
    {
        return tlPrWeapons;
    }
    /**
     * @param tlPrWeapons The tlPrWeapons to set.
     */
    public void setTlPrWeapons(int tlPrWeapons)
    {
        this.tlPrWeapons = tlPrWeapons;
    }

    
}
