package net.doublemalt.skrupel;

import java.io.Serializable;

public class WormHole  implements Serializable
{
    public enum Type
    {
        Unknown,
        Stable,
        Unstable
    }
    
    private Long id = null;
    
    private int coordX = 0;
    private int coordY = 0;
    private Type type = Type.Unknown;
    private WormHole otherSide = null;
    private boolean artificial = false;
    /**
     * @return Returns the artificial.
     */
    public boolean isArtificial()
    {
        return artificial;
    }
    /**
     * @param artificial The artificial to set.
     */
    public void setArtificial(boolean artificial)
    {
        this.artificial = artificial;
    }
    /**
     * @return Returns the coordX.
     */
    public int getCoordX()
    {
        return coordX;
    }
    /**
     * @param coordX The coordX to set.
     */
    public void setCoordX(int coordX)
    {
        this.coordX = coordX;
    }
    /**
     * @return Returns the coordY.
     */
    public int getCoordY()
    {
        return coordY;
    }
    /**
     * @param coordY The coordY to set.
     */
    public void setCoordY(int coordY)
    {
        this.coordY = coordY;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the otherSide.
     */
    public WormHole getOtherSide()
    {
        return otherSide;
    }
    /**
     * @param otherSide The otherSide to set.
     */
    public void setOtherSide(WormHole otherSide)
    {
        this.otherSide = otherSide;
    }
    /**
     * @return Returns the type.
     */
    public Type getType()
    {
        return type;
    }
    /**
     * @param type The type to set.
     */
    public void setType(Type type)
    {
        this.type = type;
    }
    
    

}
